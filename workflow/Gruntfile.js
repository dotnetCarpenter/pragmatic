module.exports = function(grunt) {
  // grunt.registerTask('custom', 'Add a custom task to grunt', function() {
  //   ;
  // }
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    cssmin: {
      compress: {
        files: {
          '<%= pkg.baseDir %>style.min.css': ['<%= pkg.baseDir %>css/shame.css', '<%= pkg.baseDir %>style.css'/*, '<%= pkg.baseDir %>css/peer.css'*/]
        }
      }
    },
    concat: {
      options: {
        stripBanners: true
      },
      dist: {
        src: ['<%= pkg.baseDir %>css/shame.css', '<%= pkg.baseDir %>style.css'/*, '<%= pkg.baseDir %>peer.css'*/] // peer maintains his own css
      , dest: '<%= pkg.baseDir %>style.pack.css'
      }
    },
    watch: {
      css: {
        files: '<%= pkg.baseDir %>style.css',
        tasks: ['concat']
      },
      design: {
        files: {
          src: ['../design/screens/*'],
          filter: 'isFile',
          options: {
            nospawn: true,
            nonull: true
          }
        },
        tasks: ['screens']
      }
    }
    // copy: {
    //   main: {
    //     files: [
    //       {expand: true, cwd:['prototypes/scripts/'], src: ['white-space.min.js'], dest: '../../<%= pkg.baseDir %>/scripts/', filter: 'isFile'}
    //     ]
    //   }
    // }
  });

  // Load plugins
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  // grunt.loadNpmTasks('grunt-contrib-copy');

  // Default task(s).
  grunt.registerTask('default', ['cssmin']);

  // watch for design screens
  grunt.registerTask('screens', 'design screens', function(a) {
    console.dir(this);
    // for(var i = 0, len = arguments.length; i < len; i++) {
    //   grunt.log.writeln(arguments[i]);
    // }
    return false;
  });
  
}
