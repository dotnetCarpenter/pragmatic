<div class="entry-summary">
<?php the_excerpt( sprintf(__( 'continue reading %s', 'pragmatic' ), '<span class="meta-nav">&rarr;</span>' )  ); ?>
<?php
  if(is_search()) : ?>
    <p><a href="<?php the_permalink(); ?>">Læs mere &gt;&gt;</a></p>
  <?php
    wp_link_pages('before=<div class="page-link">' . __( 'Pages:', 'pragmatic' ) . '&after=</div>');
  endif;
?>
</div> 