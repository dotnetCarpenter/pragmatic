<?php get_header(); ?>

<div class="teaser">
  <div class="grid__item one-twelfth"></div>
    <div class="grid__item seven-twelfths">
        <h1>
            <?php _e( 'Category Archives:', 'pragmatic' ) ?>
            <span><?php single_cat_title() ?></span>
        </h1>
    </div>
    <div class="grid__item four-twelfth"></div>
</div>

<div class="grid__item one-twelfth"></div>
<?php
// $number_of_posts = 0;
// add_action('loop_start', 'get_number_of_posts');//the_post
// function get_number_of_posts($wp_query) {
//     global $number_of_posts;
//     //var_dump($posts);
//     $number_of_posts = $wp_query -> post_count;
//     remove_action('loop_start', 'get_number_of_posts');
// }
global $wp_query;
the_post(); // get $wp_query to query the db for posts
rewind_posts(); //newest posts first
$posts = $wp_query->posts;
function odd_set($start, $array) {
    $ret = array();
    $how_many = count($array)-1;
    if( $how_many < $start) {
        return $ret;
    }
    foreach (range($start, $how_many, 2) as $int) {
        $ret[] = $array[$int];
    }
    return $ret;
}
?>
<div class="grid__item special-one-third">
<?php
$entries1 = odd_set(0, $posts);
foreach ($entries1 as $odd_post) :
    setup_postdata($odd_post);
    get_template_part( 'entry' );
endforeach;
?>
</div>
<div class="grid__item special-one-third">
<?php

$entries2 = odd_set(1, $posts);
foreach ($entries2 as $even_post) :
    setup_postdata($even_post);
    get_template_part( 'entry' );
endforeach; 
?>
</div>

<div class="grid__item special-one-third">
    <?php get_sidebar(); ?>
</div>
<div class="grid__item one-twelfth"></div>

<?php get_footer(); ?>