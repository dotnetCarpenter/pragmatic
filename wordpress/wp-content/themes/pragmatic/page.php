<?php
  get_header();
  if(is_front_page()) :
?>
  <section class="hero">
    
    <div class="cycle-slideshow"
      data-cycle-fx="scrollHorz"
      data-cycle-timeout="8000"
      data-cycle-slides="> .slider"
      data-cycle-pager="nav.cycle-pager"
      data-cycle-swipe=true>
  <?php
    // echo '<!-- wp_get_nav_menu_items ';
    // var_dump(wp_get_nav_menu_items('ydelser'));
    // echo '-->';
    global $post;
    $tmp_post = $post;
    $slider_posts = get_posts( array('category' => 3) );
    #$number_of_posts = count($slider_posts);
    foreach ($slider_posts as $post) : setup_postdata($post);
    // echo '<!-- $post ';
    // var_dump($post);
    // echo '-->';
  ?>
      <div class="slider">
        <div class="grid__item one-twelfth">&nbsp;</div>
        <?php if ( has_post_thumbnail() ) : ?>
        <div class="grid__item six-twelfths">
          <?php the_content(); ?>
        </div>
        <div class="grid__item one-twelfth"></div>
        <div class="grid__item three-twelfths">
          <?php
            the_post_thumbnail();
          ?>
        </div>
        <?php else: ?>
        <div class="grid__item ten-twelfths">
          <?php the_content(); ?>
        </div>
        <?php endif; ?>
        <div class="grid__item one-twelfth">&nbsp;</div>
      </div>
  <?php   
    endforeach;
    $post = $tmp_post;
    unset($tmp_post);
  ?>
    </div><!-- end slider -->
    <nav class="cycle-pager grid__item center"></nav>
        <!-- might need ontouchstart="" on spans for touch devices -->
  </section>

  <div class="teaser">
    <div class="grid__item one-twelfth"></div>
      <div class="grid__item seven-twelfths palm--ten-twelfths">
          <h2><?php category_description() ? category_description() : the_title(); ?></h2>
      </div>
      <div class="grid__item four-twelfth palm--none"></div>
  </div>
<?php
    endif; // end if is_front_page() call
    the_post();
?>

<div class="grid__item one-twelfth"></div>
<div class="grid__item seven-twelfths palm--ten-twelfths">
  <section class="panel lifted">
  <?php if(!is_front_page()) : ?>
    <h1><?php the_title(); ?></h1>
  <?php
    endif;
    the_content();
  ?>
    <?php edit_post_link(  __('Edit This'), '<div class="edit-link">', '</div>' ) ?>
  </section>
</div>
<div class="grid__item one-twelfth palm--only"></div>
<div class="grid__item one-twelfth palm--only"></div>
<div class="grid__item three-twelfths palm--ten-twelfths">
  <?php get_sidebar(); ?>
</div>
<div class="grid__item one-twelfth"></div>

<?php get_footer(); ?>