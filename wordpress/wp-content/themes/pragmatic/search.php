<?php get_header(); ?>
<div class="teaser">
  <div class="grid__item one-twelfth"></div>
    <div class="grid__item seven-twelfths palm--ten-twelfths">
        <h2 class="page-title"><?php printf( __( 'Search Results for: %s', 'pragmatic' ), '<span>' . get_search_query()  . '</span>' ); ?></h2>
    </div>
    <div class="grid__item four-twelfth palm--none"></div>
</div>

<div class="grid__item one-twelfth"></div>
<div class="grid__item six-twelfths palm--ten-twelfths">
  <?php if ( have_posts() ) : ?>
    <?php get_template_part( 'nav', 'above' ); ?>
    <?php while ( have_posts() ) : the_post() ?>
      <?php get_template_part( 'entry' ); ?>
    <?php endwhile; ?>
    <?php get_template_part( 'nav', 'below' ); ?>
  <?php else : ?>
      <div id="post-0" class="post no-results not-found">
        <h2 class="entry-title"><?php _e( 'Nothing Found', 'pragmatic' ) ?></h2>
        <div class="entry-content">
          <p><?php _e( 'Sorry, nothing matched your search. Please try again.', 'pragmatic' ); ?></p>
          <?php get_search_form(); ?>
        </div>
      </div>
  <?php endif; ?>
</div>
<div class="grid__item one-twelfth"></div>
<div class="grid__item one-twelfth palm--only"></div>
<div class="grid__item three-twelfths palm--ten-twelfths">
  <?php get_sidebar(); ?>
</div>
<div class="grid__item one-twelfth"></div>
<?php get_footer(); ?>