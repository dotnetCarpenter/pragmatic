<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title><?php wp_title(' | ', true, 'right'); ?></title>
  <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
  <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,regular,italic,600,600italic|Oswald:300,300italic,regular,italic,600,600italic&#038;subset=latin' type='text/css' media='all' />
    <!-- Brug  <?php echo get_stylesheet_directory_uri(); ?>  i stedet for direkte URL -->
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/peer.css?ver=1.0" />
  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
  <div class="beam palm--none"></div>

  <div class="grid">
    <header>
      <div class="grid__item one-twelfth"></div>      
      <div class="grid__item seven-twelfths palm--ten-twelfths">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>"
           title="<?php bloginfo( 'name' ) ?>" rel="home">
          <div class="logo">
            <div class="logo_body palm--none">
              <h1><?php bloginfo( 'name' ) ?></h1>
              <p><?php bloginfo( 'description' ) ?></p>
            </div>
            <img class="lap--none desk--none" src="<?php bloginfo('template_directory'); ?>/images/pragmatic_logo.v1.png" alt="Pragmatic - Effektiv salgs- og forretningsudvikling" />
          </div>
        </a>
      </div>
      <div class="grid__item three-twelfths palm--none">
        <?php get_search_form(); ?>
      </div>
      <div class="grid__item one-twelfth"></div>
    </header>

    <nav>
      <div class="grid__item one-twelfth"></div>
      <div class="grid__item ten-twelfths">
        <div class="search-wrapper palm--only grid__item five-tenths">
          <?php get_search_form(); ?>
        </div>
        <a class="palm--only grid__item menu-icon five-tenths">
          <span class="menu-name">MENU</span>&nbsp;<span class="menu-sign">&#9776;</span>
        </a>
        <!-- eller wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); -->
        <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
      </div>
      <div class="grid__item one-twelfth"></div>
    </nav>