  <footer>
    <div class="grid__item one-twelfth"></div>
    <div class="grid__item special-one-third">
    <?php // var_dump(wp_get_sidebars_widgets());
      if ( is_active_sidebar( 'pragmatic-footer-widget-area' ) ) :
        dynamic_sidebar( 'pragmatic-footer-widget-area' );
      endif;    
     ?>
    </div>

    <div class="grid__item special-one-third">
    <?php
      if ( is_active_sidebar( 'pragmatic-footer-widget-area-2' ) ) :
        dynamic_sidebar( 'pragmatic-footer-widget-area-2' );
      endif;
    ?>
    </div>

    <div class="grid__item special-one-third">
    <?php
      if ( is_active_sidebar( 'pragmatic-footer-widget-area-3' ) ) :
        dynamic_sidebar( 'pragmatic-footer-widget-area-3' );
      endif;
    ?>
    </div>
    <div class="grid__item one-twelfth"></div>
  </footer>
  <div class="beam palm--none split">
    <div class="grid__item one-twelfth"></div>
    <div class="grid__item ten-twelfths">
      <span class="split__title">&copy;&nbsp;
        <?php
          echo date('Y');
          echo '&nbsp;';
          echo esc_html(bloginfo( 'name' ));
        ?>
      </span>
      <span>Powered by <a href="//wordpress.org/">WordPress</a></span>
    </div>
    <div class="grid__item one-twelfth"></div>
  </div><!-- end div class="beam" -->
</div><!-- end div class="grid" -->
<?php wp_footer();
# example left for later..
#echo sprintf( __( '%1$s %2$s %3$s. All Rights Reserved.', 'blankslate' ), '&copy;', date('Y'), esc_html(get_bloginfo('name')) ); echo sprintf( __( ' Theme By: %1$s.', 'blankslate' ), '<a href="http://tidythemes.com/">TidyThemes</a>' );
?>
</body>
</html>