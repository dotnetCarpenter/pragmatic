/**
 * style does what CSS is supposed to do.
 * @version  13.6.2
 */

// namespace IT-Kollektivet (itk)
var itk = itk || {};

(function (win, doc, itk){
itk.utils = {
  nada: function(e) {
    e.preventDefault();
    return false;
  },
  plugins: {
    delay: function(fn, wait) {
      var self = this;
      setTimeout(function(){ fn.call(self, self[0]); }, wait);
      return this;
    },
    defer: function(fn) {
      return this.delay(fn, 1);
    },
    move: function(x, y) {
      var translate = ['translate(',x || 0,'px,',y || 0,'px)'];
      return this.each(function() {
        this.style[Modernizr.prefixed('transform')] = translate.join('');
      });
    },
    do: function(fn) {
      var self = this;
      return this.each(function(i) {
        // this (self) will be jQuery with what's selected,
        // the first argument (i), the integer index of the selected set
        // the second argument (this), the DOM element in the selected set
        fn.call(self, i, this);
      });
    }
  },
  $: win.jQuery || win.Zepto
};
//itk.utils.$.fn.defer = itk.utils.defer;
itk.utils.$.extend(itk.utils.$.fn, itk.utils.plugins);
itk.toggleMobileMenu = function() {
  var $ = itk.utils.$,
      $menuIcon = $('nav .menu-icon'),
      $search = $('nav .search-wrapper');
  if(this.className.indexOf('menu-icon') > -1) {
    $search
      .removeClass('active five-tenths')
      .addClass('inactive one-tenth')
      .bind('submit', itk.utils.nada);
    $menuIcon
      .addClass('active nine-tenths')
      .removeClass('inactive')
      .parent()
      .defer(function() {
        var $menuName = $('nav .menu-name');
        // console.log('menu-icon', -this.width());
        // console.log('search-wrapper', $search.width());
        // console.log('menu-name', $menuName.width());
        $menuName.move( -this.width() + $menuName.width() + 54/*magic number*/ );
      });
  } else if(this.className.indexOf('search-wrapper') > -1) {
    $menuIcon
      .removeClass('active five-tenths nine-tenths')
      .addClass('inactive two-tenths')
      .find('.menu-name')
      .move(0);
    $search.addClass('active eight-tenths')
      .removeClass('inactive one-tenth')
      .delay(function() {
        this.unbind('submit', itk.utils.nada);
      },1000)
      .find('.s')
      .focus();
  } else {
    $menuIcon.removeClass('active inactive two-tenths nine-tenths').addClass('five-tenths');
    $search.removeClass('active inactive one-tenth eight-tenths').addClass('five-tenths');
  }
};
// on DOMready
itk.utils.$(function($) {
  $('nav .menu-icon, nav .search-wrapper, header, nav ~ *').on('touchstart',itk.toggleMobileMenu);
});
}(window, document, itk));