var utils = {
	seq: function() {
		function next(){
			var args = utils.args.call(arguments);
			var currentFn = this.shift();
			if(!currentFn) return;	// done
			currentFn.apply(promise, args);
		}
		var promise = next.bind(utils.args.call(arguments));
		return promise;
	}
  , args: function() {
		return Array.prototype.splice.call(this, 0, this.length);
	}
}